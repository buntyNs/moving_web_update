<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'HomeController@index');
Route::get('/about-us', 'AboutController@index');
Route::get('/contact-us', 'ContactController@index');

Route::get('/services', 'ServiceController@index');

Route::get('/services/residential', 'ServiceController@residential');

Route::get('/services/commercial', 'ServiceController@commercial');

Route::get('/services/packing', 'ServiceController@packing');

Route::get('/services/senior', 'ServiceController@senior');

