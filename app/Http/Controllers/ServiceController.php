<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function index()
    {
        return view("services");
    }
    
    public function residential()
    {
        return view("services.residential");
    }

    public function packing()
    {
        return view("services.packing");
    }

    public function senior()
    {
        return view("services.senior");
    }

    public function commercial()
    {
        return view("services.commercial");
    }
    

}
