<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $email;
    public $sub;
    public $mess;

    public function __construct($email,$subject,$message)
    {
        $this->email = $email;
        $this->sub = $subject;
        $this->mess = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('sendEmail')
            ->with([
                'email' => $this->email,
                'message'=> $this->mess,
            ])->subject($this->sub);
    }
}
