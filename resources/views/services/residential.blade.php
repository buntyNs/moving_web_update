@extends("master")

@section("content")

	<!-- 
			=============================================
				Theme Inner Banner 
			============================================== 
			-->
			<div class="inner-page-banner inner-page-banner-resi">
				<div class="opacity">
					<h1>Residential Services</h1>
					<ul>
						<li><a href="/">Home</a></li>
						<li>/</li>
						<li>Service Details</li>
					</ul>
				</div> <!-- /.opacity -->
			</div> <!-- /inner-page-banner -->



			<!-- 
			=============================================
				Our Service Details
			============================================== 
			-->
			<div class="service-details">
				<div class="container">
					<div class="box-wrapper">
						<div class="title clearfix">
						<a href="/contact-us" class="loan float-right">Click here for a free quote!</a>
							<h3 class="float-left"><b>TO COMPLETE SOLUTIONS FOR YOUR MOVE</b></h3>
						
						</div> <!-- /.title -->
						<div class="top-text">
							<h4>We specialize in residential moves, small or large, relocating across the country or across town! We move entire households, apartments, condominiums or even just a few items. Our attention to detail as well as the quality of our services we feel very confident that we will meet and or exceed your expectations. Moving Professionals 4U handles your entire job from beginning to end. You will find that we are not your average moving company, we not only conduct your move, but we provide intense moving management. We stand by our continuing commitment in providing our customers with an outstanding, stress free moving experience.</h4>
							<div class="row">
								<!-- <div class="col-md-6 wow fadeInLeft"><p>Sometimes this is airony promoting  businesse product and services, because for fact that you want make ur business recognizable & earn sales of course you need with lot of effort.</p></div>
								<div class="col-md-6 wow fadeInRight"><p>Sometimes this is airony promoting  businesse product and services, because for fact that you want make ur business recognizable & earn sales of course you need with lot of effort.</p></div> -->
							</div>
						</div> <!-- /.top-text -->

						<div class="middle-text list-box-text wow fadeInUp">
							<h3>Included Featues</h3>
							<p>We offer the RIGHT KIND OF HELP……..exactly when you need it.</p>
							<ul>
								<li>
									<h4>Custom Packing</h4>
									<p>We provide packing services for simply one specialty item, such as a television, mirror or wall hangings, or we can pack your entire household. Standard and specialty cartons are included in providing this professional care service.</p>
								</li>
								<li>
									<h4>Experienced Packers</h4>
									<p>Our packing teams are experienced household packing professionals.</p>
								</li>
								<li>
									<h4>Bulky Item Moving</h4>
									<p>We move pianos, safes, workshop tools, trampolines, yard equipment, etc.  We have specialized equipment for the handling these items.</p>
								</li>
								<li>
									<h4>Pad Wrapped Service</h4>
									<p>Our crews will take great care of your belongings by padding and wrapping each item.  Your load will be secured with straps, and load bars when required</p>
								</li>

								<li>
									<h4>Fragile Items / Antiques</h4>
									<p>Packing delicate items requires reinforced cartons to provide protection. We offer specialty packing and crating for Fragile Items and also off Antique packaged as well.</p>
								</li>

							</ul>
						</div> <!-- /.middle-text -->

						<div class="bottom-text list-box-text wow fadeInUp">
							<h3>Save Your Time</h3>
							<p>Call us to speak with one of our <b>MOVING MANAGERS</b> to discuss your options and requirements. <h6 class="p-color">Contact : (702) 274-9338</h6>
							</p>
							
							<div class="row">
								<div class="col-md-6">
									<ul>
										<li>
											<h4>Experienced staff and movers</h4>
										</li>
									</ul>
								</div>
								<div class="col-md-6">
									<ul>
										<li>
											<h4>Employee Relocation packages</h4>
										</li>
									</ul>
								</div>
								<div class="col-md-6">
									<ul>
										<li>
											<h4>Full & partial packing</h4>
											
										</li>
									</ul>
								</div>
								<div class="col-md-6">
									<ul>
										<li>
											<h4>Full Value Replacement Insurance</h4>
										</li>
									</ul>
								</div>

								<div class="col-md-6">
									<ul>
										<li>
											<h4>Full Value Replacement Insurance</h4>
										</li>
									</ul>
								</div>

								<div class="col-md-6">
									<ul>
										<li>
											<h4>Complete assembly, disassembly and placement of furniture and boxes</h4>
										</li>
									</ul>
								</div>

								<div class="col-md-6">
									<ul>
										<li>
											<h4>Upholstery shrink wrapping</h4>
										</li>
									</ul>
								</div>

								<div class="col-md-6">
									<ul>
										<li>
											<h4>Specialty moving - pianos, safes, hot tubs, playground equipment, etc.</h4>
										</li>
									</ul>
								</div>

								<div class="col-md-6">
									<ul>
										<li>
											<h4>Floor protection for carpet, tile, hardwood flooring, etc.</h4>
										</li>
									</ul>
								</div>

								<div class="col-md-6">
									<ul>
										<li>
											<h4>On truck storage to accommodate home closing dates</h4>
										</li>
									</ul>
								</div>

								<div class="col-md-6">
									<ul>
										<li>
											<h4>Safe, expert handling of all your household goods</h4>
										</li>
									</ul>
								</div>

								<div class="col-md-6">
									<ul>
										<li>
											<h4>Competitive pricing!</h4>
										</li>
									</ul>
								</div>
							</div>
						</div> <!-- /.bottom-text -->
					</div> <!-- /.box-wrapper -->
				</div> <!-- /.container -->
			</div> <!-- /.service-details -->
@endsection

