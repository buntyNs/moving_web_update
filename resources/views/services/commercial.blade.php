@extends("master")

@section("content")

	<!-- 
			=============================================
				Theme Inner Banner 
			============================================== 
			-->
			<div class="inner-page-banner inner-page-banner-com">
				<div class="opacity">
					<h1>Commercial Services</h1>
					<ul>
						<li><a href="/">Home</a></li>
						<li>/</li>
						<li>Service Details</li>
					</ul>
				</div> <!-- /.opacity -->
			</div> <!-- /inner-page-banner -->



			<!-- 
			=============================================
				Our Service Details
			============================================== 
			-->
			<div class="service-details">
				<div class="container">
					<div class="box-wrapper">
						<div class="title clearfix">
							<h3 class="float-left">Office & Commercial Moving</h3>
							<a href="/contact-us" class="loan float-right">Click here for a free quote!</a>
						</div> <!-- /.title -->
						<div class="top-text">
							<h4>We specialize in moving homes as well as businesses, whether it be a single office and or the relocating of your employees. Moving can be an incredibly time consuming process and the key to an efficient move is the organized fashion it which the move is performed. Moving Professionals 4U allows your staff to minimize downtime by having our moving managers plan a strategic and efficient move with the direction of your coordinators.</h4>
							<div class="row">
								<!-- <div class="col-md-6 wow fadeInLeft"><p>Sometimes this is airony promoting  businesse product and services, because for fact that you want make ur business recognizable & earn sales of course you need with lot of effort.</p></div>
								<div class="col-md-6 wow fadeInRight"><p>Sometimes this is airony promoting  businesse product and services, because for fact that you want make ur business recognizable & earn sales of course you need with lot of effort.</p></div> -->
							</div>
						</div> <!-- /.top-text -->

						<div class="middle-text list-box-text wow fadeInUp">
							<ul>
								<li>
									<h4>We offer after hours moving to accommodate your schedule!</h4>
								</li>
							</ul>
						</div> <!-- /.middle-text -->

						<div class="company-seo-text">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-xs-12"><img src="/images/service_goods.png" alt=""></div>
						<div class="col-md-6 col-xs-12">
							<div class="text">
								<div class="theme-title-two">
									<h2>Specialized & Standard Packing Supplies</h2>
								</div>
								<p>

									1. 4.5 cubic feet<br/>
									2. Dish Pack - 5 cubic feet <br/>
									3. Mirror Pack <br/>
									4. File box <br/>
									5. Tape <br/>
									6. Washer Lock <br/>
									7. Packing paper <br/>
									8. Dish dividers <br/>
									9. Shrink wrap <br/>
									10. Bubble wrap <br/>
									11. 6.0 cubic feet Large Box <br/>
									12. 3.0 cubic feet Medium Box <br/>
									13. 1.5 cubic feet Small Box <br/>
									14. Wardrobe Box <br/>
								</p>
							</div>
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.company-seo-text -->

					
						
						</div> <!-- /.bottom-text -->
					</div> <!-- /.box-wrapper -->
				</div> <!-- /.container -->
			</div> <!-- /.service-details -->
			
			


@endsection