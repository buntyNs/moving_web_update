@extends("master")

@section("content")

	<!-- 
			=============================================
				Theme Inner Banner 
			============================================== 
			-->
			<div class="inner-page-banner inner-page-banner-pack">
				<div class="opacity">
					<h1>Packing Services</h1>
					<ul>
						<li><a href="/">Home</a></li>
						<li>/</li>
						<li>Service Details</li>
					</ul>
				</div> <!-- /.opacity -->
			</div> <!-- /inner-page-banner -->



			<!-- 
			=============================================
				Our Service Details
			============================================== 
			-->
			<div class="service-details">
				<div class="container">
					<div class="box-wrapper">
						<div class="title clearfix">
							<h3 class="float-left">Packing Services</h3>
							<a href="/contact-us" class="loan float-right">Click here for a free quote!</a>
						</div> <!-- /.title -->
						<div class="top-text">
							<h4>One of the most intimidating tasks required when moving a home or office is the actual physical packing of placing items in boxes or crating furniture to be moved. Determining the safest way to pack and unpack your belongings is a hassle that requires a great deal of time and attention. We can do it for you!   You may want your whole home or office packed, you may want a simple kitchen packed. Either way, ask one our Moving Managers to price this for you. </h4>
							<div class="row">
								<!-- <div class="col-md-6 wow fadeInLeft"><p>Sometimes this is airony promoting  businesse product and services, because for fact that you want make ur business recognizable & earn sales of course you need with lot of effort.</p></div>
								<div class="col-md-6 wow fadeInRight"><p>Sometimes this is airony promoting  businesse product and services, because for fact that you want make ur business recognizable & earn sales of course you need with lot of effort.</p></div> -->
							</div>
						</div> <!-- /.top-text -->

				<div class="container">
					<div class="row">
						<div class="col-md-6 col-xs-12"><img src="/images/nuts.jpg" alt=""></div>
						<div class="col-md-6 col-xs-12">
							<div class="text">
								
								<p>
								We also understand that you may not have time to pack for your move. Work and family obligations can sometimes make it difficult to dedicate the time to properly prepare and pack your items. With our assistance from our packing teams, we can take the hassle out of the entire packing process or simply take care of the “pain the neck” portions. Once you contact a Moving Professionals 4U Moving Manager, we can schedule an appointment to visit your home or office to determine the packing needs of your move.
								</p>

								<p>
								You can choose to have your entire home or office professionally packed by our team, or select specific rooms you would like packed. Once we have all the necessary information, we are ready to pack! Our team will securely shrink wrap, blanket, box, and/or crate your items to prevent damage during the moving process.
								</p>
							</div>
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
				</div>
				</div>
		

@endsection