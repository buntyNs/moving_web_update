@extends("master")

@section("content")

	<!-- 
			=============================================
				Theme Inner Banner 
			============================================== 
			-->
			<div class="inner-page-banner inner-page-banner-ar">
				<div class="opacity">
					<h1>Senior Moves</h1>
					<ul>
						<li><a href="/">Home</a></li>
						<li>/</li>
						<li>Service Details</li>
					</ul>
				</div> <!-- /.opacity -->
			</div> <!-- /inner-page-banner -->


   
			<!-- 
			=============================================
				Our Service Details
			============================================== 
			-->
			<div class="service-details">
				<div class="container">
					<div class="box-wrapper">
						<div class="title clearfix">
							<h3 class="float-left">Senior Relocation</h3>
							<a href="/contact-us" class="loan float-right">Click here for a free quote!</a>
						</div> <!-- /.title -->
						<div class="top-text">
							<h4>No matter if your elderly loved one is moving long distance or locally, you can trust our senior moving team to provide the customized moving solutions required to deliver a stress free senior relocation. You will work directly with our kind and knowledgeable staff to make informed decisions that best fit your seniors needs and budget. With our comprehensive list of a la carte options and concierge services, Moving Professionals 4U has the utmost confidence in providing you an organized and hassle free senior move.</h4>
							<div class="row">
								<div class="col-md-6 wow fadeInLeft"><p>MP4U believes you deserve top notch treatment and nothing less.  When you choose MP4U, you receive a hard working crew of moving professionals trained to handle even the most fragile belongings. We always treat our customer’s items like they were our own and strive to give you the stress-free hassle-free relocation experience you deserve.</p></div>
								<div class="col-md-6 wow fadeInRight"><p>Call one of our knowledgeable and friendly moving managers today to learn more or schedule your free moving estimate by filling out our online form.</p></div>
							</div>
						</div> <!-- /.top-text -->

						<div class="middle-text list-box-text wow fadeInUp">
							<h3>Included Featues</h3>
							<p>Achieve all your goals and aspirations; with the right kind of help, exactly when you need it.</p>
							<ul>
								<li>
									<h4>Trained & Uniformed Professionals</h4>
									
								</li>
								<li>
									<h4>Fully Licensed & Insured</h4>
									
								</li>
								<li>
									<h4>No Hidden Fees or Charges AT ALL</h4>
									
								</li>
								<li>
									<h4>Packing & Unpacking Services upon request</h4>
									

								<li>
									<h4>Flexible Scheduling (7 days a week / 24 hours a day)</h4>
									
								</li>

								<li>
									<h4>Moving Management Services</h4>
									
								</li>
							</ul>
						</div> <!-- /.middle-text -->

						<!-- <div class="bottom-text list-box-text wow fadeInUp">
							<h3>Boost your website</h3>
							<p>Any salaried, self-employed or professional Public and Privat companies, Government sector employees including Public Sector is eligible for a personal loan.</p>
							<div class="row">
								<div class="col-md-6">
									<ul>
										<li>
											<h4>Design &amp; Devlopment</h4>
											<p>Sometimes this is airony promoting  businesse product and services, because for fact that you want make ur business</p>
										</li>
									</ul>
								</div>
								<div class="col-md-6">
									<ul>
										<li>
											<h4>Content Creation</h4>
											<p>Sometimes this is airony promoting  businesse product and services, because for fact that you want make ur business</p>
										</li>
									</ul>
								</div>
								<div class="col-md-6">
									<ul>
										<li>
											<h4>Technical Service</h4>
											<p>Sometimes this is airony promoting  businesse product and services, because for fact that you want make ur business</p>
										</li>
									</ul>
								</div>
								<div class="col-md-6">
									<ul>
										<li>
											<h4>Business Services</h4>
											<p>Sometimes this is airony promoting  businesse product and services, because for fact that you want make ur business</p>
										</li>
									</ul>
								</div>
							</div>
						</div> /.bottom-text -->
					</div> <!-- /.box-wrapper -->
				</div> <!-- /.container -->
			</div> <!-- /.service-details -->
			
			


@endsection