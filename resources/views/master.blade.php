<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from creativegigs.net/html/gullu/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 06 Jan 2019 08:25:25 GMT -->
<head>
		<meta charset="UTF-8">
		<!-- For IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- For Resposive Device -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- For Window Tab Color -->
		<!-- Chrome, Firefox OS and Opera -->
		<meta name="theme-color" content="#2c2c2c">
		<!-- Windows Phone -->
		<meta name="msapplication-navbutton-color" content="#2c2c2c">
		<!-- iOS Safari -->
		<meta name="apple-mobile-web-app-status-bar-style" content="#2c2c2c">

		<title>Proffesstionl Moving Company</title>

		<!-- Favicon -->
		<link rel="icon" type="image/png" sizes="56x56" href="/images/logo-moving.png">


		<!-- Main style sheet -->
		<link rel="stylesheet" type="text/css" href="/css/style.css">
		<!-- responsive style sheet -->
		<link rel="stylesheet" type="text/css" href="/css/responsive.css">
		<link rel="stylesheet" type="text/css" href="/css/banner.css">


		<!-- Fix Internet Explorer ______________________________________-->
<!-- 
		[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="vendor/html5shiv.js"></script>
			<script src="vendor/respond.js"></script>
		<![endif] -->

			
	</head>

	<body>
		<div class="main-page-wrapper">

			<!-- ===================================================
				Loading Transition
			==================================================== -->
			<div id="loader-wrapper">
				<div id="loader">
					<ul>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
					</ul>
				</div>
			</div>

			<!-- 
			=============================================
				Theme Header
			============================================== 
			-->
			<header class="theme-menu-wrapper full-width-menu">
				<div class="header-wrapper">
					<div class="clearfix">
						<!-- Logo -->
						<div class="logo float-left tran4s" id = "logo"><a href="/"><img height = "40px" src="/images/logo-moving.png" alt="Logo"></a></div>
						<!-- <div class="logo float-left tran4s"><a href="index-2.html">Logo</a></div> -->


						<!-- ============================ Theme Menu ========================= -->
						<nav class="theme-main-menu float-right navbar" id="mega-menu-wrapper">
							<!-- Brand and toggle get grouped for better mobile display -->
						   <div class="navbar-header">
						     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
						       <span class="sr-only">Toggle navigation</span>
						       <span class="icon-bar"></span>
						       <span class="icon-bar"></span>
						       <span class="icon-bar"></span>
						     </button>
						   </div>
						   <!-- Collect the nav links, forms, and other content for toggling -->
						   <div class="collapse navbar-collapse" id="navbar-collapse-1">
								<ul class="nav">
									<li class="dropdown-holder menu-list "><a href="/" class="tran3s">Home</a>
									</li>

                                    <li class="dropdown-holder menu-list "><a href="javascript:void(0)" class="tran3s">Services</a>
                                    <ul class="sub-menu">
											<li><a href="/services/residential">Residential Services</a></li>
											<li><a href="/services/commercial">Commercial Services</a></li>
											<li><a href="/services/packing">Packing Services</a></li>
                                            <li><a href="/services/senior"> Senior Moves</a></li>
										</ul>
									</li>

                                    <li class="dropdown-holder menu-list "><a href="/about-us" class="tran3s">About us</a>
									</li>

                                    <li class="dropdown-holder menu-list "><a href="/contact-us" class="tran3s">Contact us</a>
									</li>
								</ul>
						   </div><!-- /.navbar-collapse -->
						</nav> <!-- /.theme-main-menu -->
					</div> <!-- /.clearfix -->
				</div>
			</header> <!-- /.theme-menu-wrapper -->
			
			<!-- 
			=============================================
				Theme Main Banner
			============================================== 
			-->
		
            @yield("content")
			
			<!-- 
			=============================================
				Footer
			============================================== 
			-->
			<footer class="bg-one">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12">
							<div class="footer-logo">
								<a href="index-2.html"><img height = "40px" src="/images/logo-moving.png" alt="Logo"></a>
								<h5><a href="#" class="tran3s">Movingprofessionals4u@gmail.com</a></h5>
								<h6 class="p-color">(702) 274-9338</h6>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12 footer-list">
							<h4>Quick Links</h4>
							<ul>

                            <li><a class="tran3s" href="/services/residential">Residential Services</a></li>
							<li><a class="tran3s" href="/services/commercial">Commercial Services</a></li>
						    <li><a class="tran3s" href="/services/packing">Packing Services</a></li>
                            <li><a class="tran3s" href="/services/senior">Senior Moves</a></li>

							</ul>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12 footer-list">
							<h4>About Us</h4>
							<ul>
								<li><a href="#" class="tran3s">Our Story</a></li>
								<li><a href="#" class="tran3s">Our Services</a></li>
								<li><a href="#" class="tran3s">Contact</a></li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12 footer-subscribe">
							<h4>Subscribe Us</h4>
							<form action="#">
								<input type="text" placeholder="Enter your mail">
							</form>
							<ul>
								<li><a href="#" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#" class="tran3s"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
								<li><a href="#" class="tran3s"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div> <!-- /.row -->

					<div class="bottom-footer clearfix">    
						<p class="float-left">&copy; 2017 <a href="#" class="tran3s p-color">movingprofessionals4u</a>. All rights reserved</p>
					</div>
				</div> <!-- /.container -->
			</footer>


			<!-- Sign-Up Modal -->
			<div class="modal fade signUpModal theme-modal-box" role="dialog">
				<div class="modal-dialog">
				    <!-- Modal content-->
				    <div class="modal-content">
					    <div class="modal-body">
					        <h3>Login with Social Networks</h3>
					        <ul class="clearfix">
					        	<li class="float-left"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i> facebook</a></li>
					        	<li class="float-left"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i> Google</a></li>
					        	<li class="float-left"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a></li>
					        	<li class="float-left"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> Linkedin</a></li>
					        </ul>
					        <form action="#">
					        	<h6><span>or</span></h6>
					        	<div class="wrapper">
					        		<input type="text" placeholder="Username or Email">
					        		<input type="password" placeholder="Password">
					        		<ul class="clearfix">
										<li class="float-left">
											<input type="checkbox" id="remember">
											<label for="remember">Remember Me</label>
										</li>
										<li class="float-right"><a href="#" class="p-color">Lost Your Password?</a></li>
									</ul>
									<button class="p-bg-color hvr-trim-two">Login</button>
					        	</div>
					        </form>
					    </div> <!-- /.modal-body -->
				    </div> <!-- /.modal-content -->
				</div> <!-- /.modal-dialog -->
			</div> <!-- /.signUpModal -->

	        

	        <!-- Scroll Top Button -->
			<button class="scroll-top tran3s">
				<i class="fa fa-angle-up" aria-hidden="true"></i>
			</button>

			<div id="svag-shape">
				<svg height="0" width="0">
					<defs>
					    <clipPath id="shape-one">
					     	<path fill-rule="evenodd"  fill="rgb(168, 168, 168)"
					 d="M343.000,25.000 C343.000,25.000 100.467,106.465 25.948,237.034 C-30.603,336.119 15.124,465.228 74.674,495.331 C134.224,525.434 212.210,447.071 227.280,432.549 C242.349,418.028 338.889,359.517 460.676,506.542 C582.737,653.896 725.650,527.546 751.000,478.000 C789.282,403.178 862.636,-118.314 343.000,25.000 Z"/>
					    </clipPath>
					</defs>
				</svg>
			</div>
			


		<!-- Js File_________________________________ -->

		<!-- j Query -->
		<script type="text/javascript" src="/vendor/jquery.2.2.3.min.js"></script>
		<!-- Bootstrap Select JS -->
		<script type="text/javascript" src="/vendor/bootstrap-select/dist/js/bootstrap-select.js"></script>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="/vendor/bootstrap/bootstrap.min.js"></script>

		<!-- Vendor js _________ -->
		<!-- Camera Slider -->
		<script type='text/javascript' src='/vendor/Camera-master/scripts/jquery.mobile.customized.min.js'></script>
	    <script type='text/javascript' src='/vendor/Camera-master/scripts/jquery.easing.1.3.js'></script> 
	    <script type='text/javascript' src='/vendor/Camera-master/scripts/camera.min.js'></script>
	    <!-- Mega menu  -->
		<script type="text/javascript" src="/vendor/bootstrap-mega-menu/js/menu.js"></script>
		
		<!-- WOW js -->
		<script type="text/javascript" src="/vendor/WOW-master/dist/wow.min.js"></script>
		<!-- owl.carousel -->
		<script type="text/javascript" src="/vendor/owl-carousel/owl.carousel.min.js"></script>
		<!-- js count to -->
		<script type="text/javascript" src="/vendor/jquery.appear.js"></script>
		<script type="text/javascript" src="/vendor/jquery.countTo.js"></script>
		<!-- Fancybox -->
		<script type="text/javascript" src="/vendor/fancybox/dist/jquery.fancybox.min.js"></script>

		<!-- Theme js -->
		<script type="text/javascript" src="/js/theme.js"></script>

		</div> <!-- /.main-page-wrapper -->

		<script>
		$(document).ready(function() {
			$('#contact').submit(function(e) {
				var flag = 0;
				e.preventDefault();
				$('.require').each(function() {
					if ($.trim($(this).val()) == '') {
						$(this).css("border", "1px solid red");
						e.preventDefault();
						flag = 1;
					} else {
						$(this).css("border", "1px solid grey");
						flag = 0;
					}
				});

				if (flag == 0) {
					$.ajax({
						url: '/send-contact-mail',
						type: 'POST',
						data: $("#contact").serialize(),
					})
							.done(function(data) {
								// $("#success").show();
								// $('#contact').reset();
								alert(data);
							})
							.fail(function() {
								alert('Ajax Submit Failed ...');
							});
				}
			});
		});
		</script>
	</body>

<!-- Mirrored from creativegigs.net/html/gullu/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 06 Jan 2019 08:26:29 GMT -->
</html>