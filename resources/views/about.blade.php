@extends("master")

@section("content")

<div class="inner-page-banner inner-page-banner-ab" style = "margin-bottom : 0" >
				<div class="opacity" style = "height:50%;background:rgba(0, 0, 10, 0.07);" >
				
					
					<ul>
						<li><a href="/"></a></li>
						<li></li>
						<li></li>
					</ul>
				</div> <!-- /.opacity -->

			</div> <!-- /inner-page-banner -->
			<div>
			


			<!-- 
			=============================================
				About Text
			============================================== 
			-->


            <div class="company-seo-text" style = "margin-top : 100px">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-xs-12"><img src="/images/home/12.jpg" alt=""></div>
						<div class="col-md-6 col-xs-12">
							<div class="text">
								<div class="theme-title-two">
									<h2>Moving Managers That Truly Care</h2>
								</div>
								<p>
								Moving Professionals 4U is DIFFERENT.  We know how stressful moving day can be - but it doesn’t have to be. Every single team member is highly trained and  <I><B>truly does care</B></I> about the happiness of our customers. From our Moving Management Teams to our Operations Teams, our people are top quality individuals.  This is where you will find the biggest difference. Experience the difference that years of service and years of perfecting the personalized touch can mean for you and your family.
								</p>
							</div>
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
            </div> <!-- /.company-seo-text -->
            
            <div class="company-seo-text" style = "margin-top : 100px">
				<div class="container">
					<div class="row">
						
						<div class="col-md-6 col-xs-12">
							<div class="text">
								<div class="theme-title-two">
									<h2>Timely  Arrival & Delivery</h2>
								</div>
								<p>All of us here at Moving Professionals 4U respects your time. Our moving professionals show up on the time provided / requested. We deliver on time, it’s as simple as that. There is no “window”. You simply pick the day you want to move and that is the day we pick up. We believe we are only as good as our word. So if we say we will be at your home or office ready to move between 9am and 11am on Monday, then that’s when we will arrive. Our movers in Moving Professionals 4U treat our customers the way we would like to be treated. If you’re not satisfied with our service, please let us know. Anything less than excellent is unacceptable.</p>
							</div>
                        </div>
                        
                        <div class="col-md-6 col-xs-12"><img src="/images/time.png" alt=""></div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
            </div> <!-- /.company-seo-text -->
            
            <div class="company-seo-text" style = "margin-top : 100px">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-xs-12"><img src="/images/guarantee.png" alt=""></div>
						<div class="col-md-6 col-xs-12">
							<div class="text">
								<div class="theme-title-two">
									<h2>Satisfaction Guaranteed</h2>
								</div>
                                <p>
								Moving Professionals 4U guarantees your satisfaction with every move and we put it in writing! The Moving Professionals 4U guarantee gives you peace of mind that comes from knowing that we have a full customer service department that will not leave any issue that could arise, unresolved. Our dedication to customer service has earned Moving Professionals 4U an A rating from The Better Business Bureau.
								</p>
							</div>
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
            </div> <!-- /.company-seo-text -->
            

            <div class="company-seo-text" style = "margin-top : 100px">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-xs-12">
							<div class="text">
								<div class="theme-title-two">
									<h2>Interstate Moving must start with a plan.</h2>
								</div>
                              <p>
							  Did you know that interstate moving (a long distance move) requires intense logistic planning. While it may appear like a big project to move, we have the experience to make it simple. It’s simple to us because we know what we are doing. Starting with the quote process, our long distance Moving Managers work with you to gather all the information required to make a decision, in addition to what is required to complete your long distance move……. <B>on budget and on time</B>. In gathering this information, our long distance Moving Managers can help answer any question you may have, as well as providing a binding estimated cost. To make things a little easier for you, you can start the process by requesting a free long distance moving quote from Moving Professionals 4U, right here within our website.

							  </p>
							</div>
						</div>
						<div class="col-md-6 col-xs-12"><img src="/images/plan.jpg" alt=""></div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.company-seo-text -->


            <div class="company-seo-text" style = "margin-top : 100px">
				<div class="container">
					<div class="row">
					<div class="col-md-6 col-xs-12"><img src="/images/home-front.jpeg" alt=""></div>
						<div class="col-md-6 col-xs-12">
							<div class="text">
								<div class="theme-title-two">
									<h2>We offer long distance storage options to keep your things safe until you need them.</h2>
								</div>
                              <p>
							  Planning a long distance move but your future home isn’t ready for move in just yet? Moving Professionals 4U has you covered! One of the main things people tend to worry about is where they can store their items during a move. This is why we offer long distance storage, short term as well as long term. Your items are completely safe with us.  Our storage, short term and long term has NO PUBLIC ACCESS. Our long distance moving facilities are specifically designed to accommodate storage for every kind of need. Whether you have a small amount of storage to be had or need to store an entire home, we have the facilities to take care of this. If you want more information about our storage service, give us a call and we can customer taylor any kind of storage need.
							  </p>
							</div>
                        </div>
                        
                      
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.company-seo-text -->


            <div class="company-seo-text" style = "margin-top : 100px">
				<div class="container">
					<div class="row">
				
						<div class="col-md-6 col-xs-12">
							<div class="text">
								<div class="theme-title-two">
									<h2>OFFICE MOVES - You take care of the business, we will take care of the move.</h2>
								</div>
                <p>
							  You have so many responsibilities at work, from managing employees, to servicing customers, to coming up with the next great plan of action before and after the move. Let our Professional Office Movers take you to your new office so that the focus can remain on conducting your business. We are a professional and experienced when it comes to Office Relocation and we have the experience that shows!  It’s no surprise that we are referred to as one of the TOP Commercial Relocation Moving Companies. Our affordable and experienced moving  will show you why people never want to go back to moving on their own.  In addition, our moving expertise allows you to focus on your current business goals and day to day operations without missing a beat. Whether you’re moving your business along with your home or you’re expanding your business to another location, Moving Professionals 4U is the definitely a moving company for companies of all sizes.
							  </p>
							</div>
                        </div>
						<div class="col-md-6 col-xs-12"><img src="/images/office.jpg" alt=""></div>
						
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.company-seo-text -->



@endsection

<script>

console.log(	document.getElementById("logo"));

</script>