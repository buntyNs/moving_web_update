@extends("master")

@section("content")

<!-- 
			=============================================
				Theme Inner Banner 
			============================================== 
			-->
			<div class="inner-page-banner inner-page-banner-sv">
				<div class="opacity">
					<h1>Our Services</h1>
					<ul>
						<li><a href="/">Home</a></li>
						<li>/</li>
						<li>Services</li>
					</ul>
				</div> <!-- /.opacity -->
			</div> <!-- /inner-page-banner -->



			<!-- 
			=============================================
				Our Service V1
			============================================== 
			-->
			<div class="service-version-one">
				<div class="container">
					<h2>We provide such a wide range of <br>business services!</h2>
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-service">
								<i class="flaticon-layers tran3s"></i>
								<p>Residential Services</p>
							
							</div> <!-- /.single-service -->
						</div> <!-- /.col- -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-service">
								<i class="flaticon-layers tran3s"></i>
								<p>Commercial Services</p>
								
							</div> <!-- /.single-service -->
						</div> <!-- /.col- -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-service">
								<i class="flaticon-layers tran3s"></i>
								<p>Packing Services</p>
								
							</div> <!-- /.single-service -->
						</div> <!-- /.col- -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="single-service">
								<i class="flaticon-layers tran3s"></i>
								<p>Senior Moves</p>
								
							</div> <!-- /.single-service -->
						</div> <!-- /.col- -->
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.service-version-one -->


@endsection